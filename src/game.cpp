#include <slice3d.h>
#include <camcontrol.h>

s3dRenderCamera* main_cam;
s3dLight* main_light;
struct RenderTargets
{
    slBox* boxL;
    slBox* boxR;
    slScalar scaling = 1;//0.125;
    //slVec2 boxsize = slVec2(0.5,1);
    slVec2 boxsize = slVec2(1); // depth box shoved offscreen

    s3dObject* sprite_obj;
    slScalar elapsed = 0;
    s3dLight* cam_light = s3dLight::Create();
    s3dRenderCamera cam_camera;

    RenderTargets ()
    {
        cam_camera.Resize(slInt2(384));

        boxL = slCreateBox(main_cam->GetColor());
        boxL->SetDims(slVec2(0,0),boxsize,255);
        boxR = slCreateBox(main_cam->GetDepth());
        boxR->SetDims(slVec2(boxsize.x,0),boxsize,255);

        s3dModelRef sprite_model = s3dSpriteModel("doesn't exist",true,1,0,1,120,true);

        //sprite_model->SetSpriteTex(frame);
        s3dMesh* mesh = sprite_model->meshes + 0;
        slTexture* junk_ambient = mesh->ambient_texref;
        slTexture* junk_diffuse = mesh->diffuse_texref;
        mesh->ambient_texref = cam_camera.GetColor().ReserveGetRaw();
        mesh->diffuse_texref = cam_camera.GetColor().ReserveGetRaw();
        if (junk_ambient) junk_ambient->Abandon();
        if (junk_diffuse) junk_diffuse->Abandon();

        sprite_obj = s3dCreateObject(sprite_model);
        sprite_obj->SetWorldTransform(GLmat4_offset(11,6.32,5.5) * GLmat4_yrotate(slDegToRad(180)) * GLmat4_scaling(GLvec4(2)));

        cam_light->color = GLcolor3(1,0,0);
        cam_light->strength = 8;
        cam_light->SetCoverageAngle(50);

        cam_camera.SetOrigin(s3dVec3(12.08,3.42,4.93));
        cam_camera.SetPitch(-20);
        cam_camera.Perspective(50,0.1,200);
        *cam_light->GetCamera() = cam_camera;

        main_light->SetShadowRes(1024);
    }
    ~RenderTargets ()
    {
        s3dLight::Destroy(cam_light);
        slDestroyBox(boxL);
        slDestroyBox(boxR);
        s3dDestroyObject(sprite_obj);
    }
    void Draw ()
    {
        s3dUpdateShadows();

        elapsed = slfmod(elapsed + slGetDelta() / 20,1);

        cam_camera.SetYaw(-45 + sin(elapsed*M_PI*2) * 30);
        cam_light->GetCamera()->SetYaw(cam_camera.GetYaw());
        cam_camera.Render();

        glBindTexture(GL_TEXTURE_2D,cam_camera.GetColor()->tex);
        glGenerateMipmap(GL_TEXTURE_2D);

        main_cam->Resize(boxsize * scaling);
        main_cam->Render();
    }
};
RenderTargets* render_targets;

void DrawGame ()
{
    render_targets->Draw();

    slDrawToScreen();
	slDrawUI();
}

slKeyBind* move_light;
void MoveLight ()
{
    *main_light->GetCamera() = *main_cam;
    main_light->SetCoverageAngle(90);
    //main_light->GetCamera()->projection_matrix = GLmat4_ortho(-1,1,-1,1,0,10);
}

slKeyBind* toggle_fog;
bool fog_enabled = false;
void ToggleFog ()
{
    fog_enabled = !fog_enabled;
    s3dSetDistanceFog(fog_enabled,0,0.1);
}

slKeyBind* say_pos;
void SayPosition ()
{
    s3dVec3 pos = main_cam->GetOrigin();
    printf("Camera Position: %lf, %lf, %lf\n",pos.x,pos.y,pos.z);
}

s3dObject* sponza_bldg;

void GameInit ()
{
    s3dInit();
    slSetAppDrawStage(DrawGame);

    main_light = s3dLight::Create();
    main_cam = new s3dRenderCamera;
    main_cam->Perspective(90,0.1,1000);
    //s3dSetShadowBias(0.1,1000);
    //s3dSetDistanceFog(true);

    main_cam->SetOrigin(s3dVec3(0,1.7,0));

    move_light = slGetKeyBind("Set Light Position", slKeyCode(SDLK_SPACE));
    move_light->onpress = MoveLight;
    MoveLight();

    toggle_fog = slGetKeyBind("Toggle Distance Fog", slKeyCode(SDLK_f));
    toggle_fog->onpress = ToggleFog;

    say_pos = slGetKeyBind("Say Camera Position", slKeyCode(SDLK_l));
    say_pos->onpress = SayPosition;

    s3dSetEnvironmentAmbientColor(GLcolor3(0.05,0.07,0.09));
    main_light->color = GLcolor3(0,0,1);//GLcolor3(1,0.95,0.9);
    main_light->strength = 12;



    render_targets = new RenderTargets;

    s3dModelRef sponza_model = s3dLoadObj("sponza-files/","sponza.obj");
	sponza_bldg = s3dCreateObject(sponza_model);
    sponza_bldg->SetWorldTransform(/*GLmat4_offset(0,0,0) * */GLmat4_scaling(GLvec4(1/100.)));
    //sponza_bldg->visible = false;
}

void GameQuit ()
{
    delete render_targets;
	s3dDestroyObject(sponza_bldg);

    move_light->onpress = NULL;
    toggle_fog->onpress = NULL;
    say_pos->onpress = NULL;

    delete main_cam;
    s3dLight::Destroy(main_light);

    s3dQuit();
    slSetAppDrawStage(NULL);
}

slScalar elapsed = 0;

void GameStep ()
{
    elapsed = slfmod(elapsed + slGetDelta(), 4);
    //sprite->world_transform = GLmat4_yrotate(slDegToRad((int)(elapsed*2) * 90)) * GLmat4_offset(0,0,0.5);
    slScalar cam_yaw = (int)elapsed * 90 - 30;
    //s3dSetCamRotations(&cam_yaw,NULL,NULL);
}

void Game ()
{
	GameInit();
    CamMovementInit();
	while (!slGetReqt())
	{
        CamMovementUpdate();
    	GameStep();
		slCycle();
	}
    CamMovementQuit();
	GameQuit();
}
