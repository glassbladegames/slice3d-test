#include <slice.h>
#include <sliceopts.h>
#include <slicefps.h>
#include <slice2d.h>

#include <game.h>

int main ()
{
	slInit("Slice3D Test");
	opInit();
	fpsInit();

	while (true)
	{
		Game();
		// Only exit if the user asked to exit. Otherwise restart the game.
		if (slGetReqt()) break;
	};

	fpsQuit();
	opQuit();
	slQuit();
};
