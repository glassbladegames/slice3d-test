ifeq ($(DEBUG),-debug)
CFLAGS += -w -g
else
CFLAGS += -O2 -fomit-frame-pointer -s
endif
CXXFLAGS = $(CFLAGS) -fpermissive

GAME_BINARY = built/s3d-test

INCLUDES += -Iinclude #-I/usr/local/include/ -I/usr/include/

LIBRARIES =\
	-lsliceopts\
	-lslicefps\
	\
	-lslice3d\
	-lbullet\
	\
	-lslice2d\
	-lbox2d \
	\
	-lslice\
	\
 	-lvorbisfile\
	-lvorbis\
	-logg\
	-lSDL2_image\
 	-lSDL2_ttf\
	-lSDL2\
	\
	$(OTHER_LIBRARIES)

#LIBDIR += -L /usr/local/lib/ -L /usr/lib/

GAME_SOURCES = $(shell find src -iname '*.cpp')
GAME_OBJS = $(patsubst %.cpp,%.o,$(GAME_SOURCES))

#WINDRES_SOURCES will be set by the calling Dockerfile if we're on Windows.
WINDRES_OBJS = $(patsubst %.rc,%.res,$(WINDRES_SOURCES))

all: $(GAME_OBJS) $(WINDRES_OBJS)
	$(CXX) $(CFLAGS) -o $(GAME_BINARY) $(INCLUDES) $(LIBDIR) $(GAME_OBJS) $(WINDRES_OBJS) $(LIBRARIES)

clean:
	@rm -rvf $(shell find . -iname '*.o')
	@rm -rvf $(shell find . -iname '*.res')

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $(INCLUDES) $< -o $@

%.res: %.rc
	x86_64-w64-mingw32-windres $< -O coff -o $@
