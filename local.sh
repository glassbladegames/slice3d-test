#!/bin/sh
set -eux
rm -rf built
docker build . -f builders/ubuntu/Dockerfile -t s3d-test-ubuntu --build-arg DEBUG="-debug"
docker create --name s3d-test-ubuntu s3d-test-ubuntu
docker cp s3d-test-ubuntu:/game/built built
docker rm s3d-test-ubuntu
cd built
./s3d-test
