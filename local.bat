rmdir /s /q built
docker build . -f builders/windows/Dockerfile -t s3d-test-windows --build-arg DEBUG="-debug"
if %errorlevel% neq 0 exit /b %errorlevel%
docker create --name s3d-test-windows s3d-test-windows
docker cp s3d-test-windows:/game/built built
docker rm s3d-test-windows
cd built
s3d-test.exe
